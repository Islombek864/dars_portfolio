<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Summary extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'summary';
    protected $fillable = [
        'id',
        'name',
        'text',
        'address',
        'number',
        'email',



    ];

}
