<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use mysql_xdevapi\Table;

class Collegues extends Model
{
    use HasFactory;
    public $timestamps=false;
    protected $table = 'collegues';
    protected $fillable = [
        'id',
        'name',
        'job',
        'text'
    ];
}
