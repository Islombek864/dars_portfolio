<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Facts extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'facts';
    protected $fillable = [
        'id',
        'image',
        'number',
        'text',
        'text_2',
    ];
}
