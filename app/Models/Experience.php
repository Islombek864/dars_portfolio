<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'experience';
    protected $fillable = [
        'id',
        'name',
        'year',
        'peace',
        'text_1',
        'text_2',
        'text_3',
        'text_4'


    ];
}
