<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\DB;

class ApiController extends Controller
{

    public function facts(){
        try {

            $facts = DB::select('select * from facts');


            return response()->json([
                'status'=> true,
                'data'=>$facts,
                'message'=>'ok'
            ]);
        }catch (Exception $exception){
            return response()->json([
                'status'=>false,
                'data'=>[],
                'message'=> $exception->getMessage()
            ]);
        }
    }

}
