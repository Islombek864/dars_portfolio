<?php

namespace App\Http\Controllers;

use App\Models\Facts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class FactsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->text){
            $search = $request->text;
//            $facts = Facts::where('text','like','%'.$request->text.'%')->get();
            $facts = DB::table('facts as f')
                ->where('f.text','like','%'.$request->text.'%')
                ->select('f.*')->get();
        }else{
            $facts = Facts::all();
            $search = '';
        }



        return view('admin.facts.index',compact('facts','search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'number'=>'required',
            'text'=>'required',
            'text_2'=>'required',
            'image'=>'required',
        ]);

        Facts::create([
            'number'=> $request->number,
            'text'=>$request->text,
            'text_2'=>$request->text_2,
            'image'=>$request->image,
        ]);

        return redirect()->route('facts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pay($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fact = Facts::find($id);
        $fact->delete();

        return redirect()->route('facts.index');

    }
}
