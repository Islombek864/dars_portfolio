@extends('admin.layouts.app')



@section("main")

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        <div class="col-8">
                            <h4 class="card-title">Facts</h4>
                        </div>

                        <div class="col-4">
                            <!-- App Search-->
                            <form action="{{ route('facts.index') }}" class="app-search py-0 d-none d-lg-block">
                                <div class="position-relative">
                                    <input type="text" class="form-control" name="text" placeholder="Search..." value="{{ $search }}">
                                    <span class="bx bx-search-alt"></span>
                                </div>
                            </form>
                        </div>
                    </div>

                    @if(count($facts) != 0)

                    <div class="table-rep-plugin">
                        <div class="table-responsive mb-0" data-pattern="priority-columns">
                            <table id="tech-companies-1" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>N</th>
                                    <th data-priority="1">Number</th>
                                    <th data-priority="1">Image</th>
                                    <th data-priority="3">Text bold</th>
                                    <th data-priority="1">Text</th>
                                    <th>
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            Create
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Create facts</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <form action="{{ route('facts.store') }}" method="post">
                                                        @csrf
                                                        @method('POST')
                                                        <div class="modal-body">
                                                            <label for="number" class="form-label">Number</label>
                                                            <input required type="number" class="form-control" id="number" name="number" placeholder="Number">

                                                            <label for="text" class="form-label">Text</label>
                                                            <input required type="text" class="form-control" id="text" name="text" placeholder="Text">

                                                            <label for="text_2" class="form-label">Simple text</label>
                                                            <input required type="text_2" class="form-control" id="text_2" name="text_2" placeholder="Simple text">

                                                            <label for="image" class="form-label">Image</label>
                                                            <input required type="text" class="form-control" id="image" name="image" placeholder="image">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($facts as $fact)
                                <tr>
                                    <th>{{ $fact->id }}</th>
                                    <td>{{ $fact->number }}</td>
                                    <td>{{ $fact->image }}</td>
                                    <td>{{ $fact->text }}</td>
                                    <td>{{ $fact->text_2 }}</td>
                                    <td>
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#delete{{ $fact->id }}">
                                            Delete
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="delete{{ $fact->id }}" tabindex="-1" aria-labelledby="delete{{ $fact->id }}" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Create facts</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <form action="{{ route('facts.destroy', $fact->id) }}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <div class="modal-body">
                                                            <h3>Are you sure?</h3>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-danger">Delete</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>

                    </div>

                    @else

                        <h1 class=" text-center">Malumot topilmadi</h1>

                    @endif

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->


@endsection
